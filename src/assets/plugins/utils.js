//Common
//"use strict";
 
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,

    timer: 3000,
    customClass: {
        title: 'title-class',
        icon: 'icon-class'
    }
});

const success = function (message) {
    Toast.fire({
        title: message,
        type: 'success'
    });
};


const error = function (message) {
    Toast.fire({
        title: message,
        type: 'error'
    });
};
const warning = function (message) {
    Toast.fire({
        title: message,
        type: 'warning'
    });
};
const info = function (message) {
    Toast.fire({
        title: message,
        type: 'info'
    });
};
const question = function (message) {
    Toast.fire({
        title: message,
        type: 'question'
    });
};











