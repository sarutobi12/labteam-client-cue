import Vue from "vue";
import VueRouter from "vue-router";
//admin
import Project from "./views/admin/project/Index.vue";


//client


// auth
import Auth from "./views/shares/auth/Auth.vue";
import Login from "./views/shares/auth/Login.vue";

//dash
import Dash from "./views/shares/dash/Dash.vue";
import Home from "./views/shares/dash/Home.vue";
import Resource from "./views/shares/dash/Training";


//upload

import Article from "./views/admin/article/Index"

import Demo from "./views/admin/demo/Index"

//Slide
import Slide from "./views/admin/slide/Index"

//Traning Aerial
import Training from "./views/admin/training/Index"

//OKR
import OKR from "./views/admin/okr/Index"

Vue.use(VueRouter);
const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      //HOME
      path: "/",
      component: Home,
      redirect: "/home",
      children: [{ path: "home", component: Home }]
    },
    {
      //HOME
      path: "/portfolio",
      component: Home,
      redirect: "/home",
      children: [{ path: "home", component: Home }]
    },

    {
      //HOME
      path: "/traning-resource",
      component: Resource,
      children: [{ path: "traning-resource", component: Resource }]
    },

    {
      //HOME
      path: "/demo",
      component: Resource,
      children: [{ path: "demo", component: Resource }]
    },
    //admin
    {
      path: "/admin",
      component: Dash,
      children: [{ path: "/admin", component: Project ,meta:{ requiresAuth: true} }]
    },
    //adminproject
    {
      path: "/admin-project",
      component: Dash,
      children: [{ path: "/admin-project", name: "Project", component: Project ,meta:{ requiresAuth: true} }]
    },
    //adminSlide
    {
      path: "/admin-slide",
      component: Dash,
      children: [{ path: "/admin-slide", name: "Carousel", component: Slide ,meta:{ requiresAuth: true} }]
    },
    
    //Login
    {
      path: "/login",
      component: Auth,
      children: [{ path: "/login", component: Login}]
    },
    //client task
    {
      path: "/article",
      component: Dash,
      children: [{ path: "/article",name: "Article", component: Article ,meta:{ requiresAuth: true} }]
    },

    {
      path: "/demoo",
      component: Dash,
      children: [{ path: "/demoo", component: Demo  }]
    },

    //Training Aerial
    {
      path: "/traning-resources",
      component: Dash,
      children: [{ path: "/traning-resources", name: "Training Resources", component: Training ,meta:{ requiresAuth: true} }]
    },
    //OKR
    {
      path: "/okr",
      component: Dash,
      children: [{ path: "/okr", name: "Objectives Key Results", component: OKR ,meta:{ requiresAuth: true} }]
    }
  ]
});

export default router;
